import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GenericDialogData, MesaModel} from '../../../models/models';
import {UtilidadesService} from '../../../services/utilidades.service';

@Component({
  selector: 'app-crear-editar-mesa',
  templateUrl: './crear-editar-mesa.component.html',
  styleUrls: ['./crear-editar-mesa.component.scss']
})
export class CrearEditarMesaComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private utilidadesService: UtilidadesService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GenericDialogData, MesaModel>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
  ) {
    if (this.data.operationType === 'ADD') {
      const ultimaMesa = this.data.payload.ultimaMesa + 1;

      this.formulario = this.formBuilder.group({
        numeroMesa: [ultimaMesa, Validators.required],
        cantidadMaximaPersonas: [0, Validators.required],
        activo: [true]
      });
    } else {

      const theMesa = this.data.payload.mesa as MesaModel;

      this.formulario = this.formBuilder.group({
        numeroMesa: [theMesa.numeroMesa, Validators.required],
        cantidadMaximaPersonas: [theMesa.cantidadMaximaPersonas, Validators.required],
        activo: [true]
      });
    }
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar(): void {
    const nuevaMesa: MesaModel = {
      ...this.formulario.value,
      mesa_estado_nombre: 'Libre'
    };

    if (nuevaMesa.cantidadMaximaPersonas <= 0) {
      this.utilidadesService.showErrorSnackBar('La cantidad maxima de personas debe ser mayor a cero');
      return;
    }

    this.dialogRef.close(nuevaMesa);
  }

  ngOnInit(): void {
  }

}
