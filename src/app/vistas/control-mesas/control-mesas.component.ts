import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {CrearEditarMesaComponent} from './crear-editar-mesa/crear-editar-mesa.component';
import {GenericDialogData, MesaModel} from '../../models/models';
import {MesaService} from '../../services/mesa.service';
import {UtilidadesService} from '../../services/utilidades.service';

@Component({
  selector: 'app-control-mesas',
  templateUrl: './control-mesas.component.html',
  styleUrls: ['./control-mesas.component.scss']
})
export class ControlMesasComponent implements OnInit {

  searchInput = '';
  listadoMesas: MesaModel[] = [];
  cargandoMesas = false;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private mesaService: MesaService,
    private utilidadesService: UtilidadesService
  ) {
  }

  goToHome(): void {
    this.router.navigate(['home']);
  }

  buscar(): void {

  }

  crearMesa(): void {
    const dialogRef = this.dialog
      .open<CrearEditarMesaComponent, GenericDialogData, MesaModel>(CrearEditarMesaComponent, {
        width: '500px',
        data: {
          operationType: 'ADD',
          payload: {
            ultimaMesa: this.obtenerUltimaMesa()
          }
        }
      });

    dialogRef.afterClosed().subscribe(nuevaMesa => {
      if (nuevaMesa) {
        this.cargandoMesas = true;

        this.mesaService
          .crearMesa(nuevaMesa)
          .then(id => {
            if (id > 0) {
              nuevaMesa.id = id;
              this.utilidadesService.showSuccessSnackBar('Mesa agregada exitosamente!');
              this.listadoMesas.push(nuevaMesa);
            }
          })
          .catch(err => {
            console.error(err);
            this.utilidadesService.showErrorSnackBar('Error al crear nueva mesa');
          })
          .finally(() => {
            this.cargandoMesas = false;
          });
      }
    });
  }

  actualizarMesa(theMesa: MesaModel): void {
    const dialogRef = this.dialog
      .open<CrearEditarMesaComponent, GenericDialogData, MesaModel>(CrearEditarMesaComponent, {
        width: '500px',
        data: {
          operationType: 'EDIT',
          payload: {
            mesa: theMesa
          }
        }
      });

    dialogRef.afterClosed().subscribe(mesaActualizada => {
      if (mesaActualizada) {
        this.cargandoMesas = true;

        this.mesaService
          .actualizarMesa(mesaActualizada)
          .then(() => {
            this.utilidadesService.showSuccessSnackBar('Mesa actualizada exitosamente!');
            theMesa.cantidadMaximaPersonas = mesaActualizada.cantidadMaximaPersonas;
            theMesa.numeroMesa = mesaActualizada.numeroMesa;
          })
          .catch(err => {
            console.error(err);
            this.utilidadesService.showErrorSnackBar('Error al actualizar mesa');
          })
          .finally(() => {
            this.cargandoMesas = false;
          });
      }
    });
  }

  private obtenerUltimaMesa(): number {
    let ultimaMesa = 0;

    for (const mesa of this.listadoMesas) {
      if (mesa.numeroMesa > ultimaMesa) {
        ultimaMesa = mesa.numeroMesa;
      }
    }

    return ultimaMesa;
  }

  ngOnInit(): void {
    this.cargandoMesas = true;

    this.mesaService
      .obtenerMesas()
      .then(mesas => {
        this.listadoMesas = mesas;
      })
      .catch(err => {
        console.log(err);
        this.utilidadesService.showErrorSnackBar('Error al obtener listado de mesas');
      })
      .finally(() => {
        this.cargandoMesas = false;
      });
  }

}
