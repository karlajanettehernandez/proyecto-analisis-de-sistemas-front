import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {GenericDialogData} from "../../../models/models";
import {UtilidadesService} from "../../../services/utilidades.service";

export interface SeleccionarTipoPagoRespuesta {
  tipoPago: string;
  monto: number;
  vuelto: number;
}

@Component({
  selector: 'app-pagar-orden',
  templateUrl: './pagar-orden.component.html',
  styleUrls: ['./pagar-orden.component.scss']
})
export class PagarOrdenComponent implements OnInit {

  montoCompra = 0;
  montoRecibido = 0;
  vuelto = 0;
  tipoDePagoSeleccionado = '';
  tipos: string[] = ['EFECTIVO', 'TARJETA VISA', 'TARJETA BAC'];
  bloquearCampoMontoRecibido = false;

  constructor(
    public dialogRef: MatDialogRef<GenericDialogData, SeleccionarTipoPagoRespuesta>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
    private utilidadesService: UtilidadesService
  ) {
    console.log(this.data.payload);
    this.montoCompra = this.data.payload.montoCompra;
    console.log(this.montoCompra);
  }

  calcularVuelto(): void {
    if (this.montoRecibido >= this.montoCompra) {
      this.vuelto = this.montoRecibido - this.montoCompra;
    } else {
      this.vuelto = 0;
    }
  }

  validarTipoPago(): void {
    this.vuelto = 0;

    if (this.tipoDePagoSeleccionado !== 'EFECTIVO') {
      this.montoRecibido = this.montoCompra;
      this.bloquearCampoMontoRecibido = true;
    } else {
      this.montoRecibido = 0;
      this.bloquearCampoMontoRecibido = false;
    }
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  pagar(): void {
    if (!this.tipoDePagoSeleccionado) {
      this.utilidadesService.showErrorSnackBar('Seleccione tipo de pago');
      return;
    }

    if (this.tipoDePagoSeleccionado === 'EFECTIVO') {
      if (!this.montoRecibido || this.montoRecibido <= 0) {
        this.utilidadesService.showErrorSnackBar('Ingrese monto recibido');
        return;
      }
    }

    this.dialogRef.close({
      tipoPago: this.tipoDePagoSeleccionado,
      monto: this.montoRecibido,
      vuelto: this.vuelto
    });
  }

  ngOnInit(): void {
  }

}
