import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TomarOrdenComponent} from './tomar-orden/tomar-orden.component';
import {RouterModule, Routes} from "@angular/router";
import {PrivateContentGuard} from "../../guards/private-content.guard";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatButtonModule} from "@angular/material/button";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatRadioModule} from "@angular/material/radio";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatCardModule} from "@angular/material/card";
import {MatSelectModule} from "@angular/material/select";
import {MatTableModule} from "@angular/material/table";
import { AgregarProductoOrdenComponent } from './agregar-producto-orden/agregar-producto-orden.component';
import { PagarOrdenComponent } from './pagar-orden/pagar-orden.component';

const routes: Routes = [
  {
    path: 'tomar-orden',
    component: TomarOrdenComponent,
    canActivate: [PrivateContentGuard]
  }
];

@NgModule({
  declarations: [TomarOrdenComponent, AgregarProductoOrdenComponent, PagarOrdenComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatToolbarModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatProgressBarModule,
    FormsModule,
    MatDialogModule,
    MatRadioModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatCardModule,
    MatSelectModule,
    MatTableModule,
  ]
})
export class OrdenesModule {
}
