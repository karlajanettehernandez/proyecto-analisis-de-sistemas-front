import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {GenericDialogData} from "../../../models/models";
import {UtilidadesService} from "../../../services/utilidades.service";

@Component({
  selector: 'app-agregar-producto-orden',
  templateUrl: './agregar-producto-orden.component.html',
  styleUrls: ['./agregar-producto-orden.component.scss']
})
export class AgregarProductoOrdenComponent implements OnInit {

  cantidad = 1;

  constructor(
    public dialogRef: MatDialogRef<GenericDialogData, number>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
    private utilidadesService: UtilidadesService
  ) {
  }

  agregar(): void {
    if (this.cantidad <= 0) {
      this.utilidadesService.showErrorSnackBar('La cantidad debe ser mayor a cero');
      return;
    }

    this.dialogRef.close(this.cantidad);
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
