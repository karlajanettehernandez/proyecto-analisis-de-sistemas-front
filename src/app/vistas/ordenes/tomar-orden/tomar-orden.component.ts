import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {UtilidadesService} from "../../../services/utilidades.service";
import {CategoriaModel, GenericDialogData, MesaModel, OrdenDetalleModel, ProductoModel} from "../../../models/models";
import {CategoriaService} from "../../../services/categoria.service";
import {ProductoService} from "../../../services/producto.service";
import {MesaService} from "../../../services/mesa.service";
import {AgregarProductoOrdenComponent} from "../agregar-producto-orden/agregar-producto-orden.component";
import {PagarOrdenComponent, SeleccionarTipoPagoRespuesta} from "../pagar-orden/pagar-orden.component";

@Component({
  selector: 'app-tomar-orden',
  templateUrl: './tomar-orden.component.html',
  styleUrls: ['./tomar-orden.component.scss']
})
export class TomarOrdenComponent implements OnInit {

  mostrarBarraProgreso = false;
  mostrarCategorias = true;

  totalOrden = 0;
  clienteNit = '';
  clienteNombre = '';
  clienteDireccion = '';

  categorias: CategoriaModel[] = [];
  productos: ProductoModel[] = [];
  mesas: MesaModel[] = [];
  categoriaActual: CategoriaModel = null;
  mesaSeleccionada: MesaModel = null;
  ordenParaLlevar = false;

  detalleOrden: OrdenDetalleModel[] = [];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private utilidadesService: UtilidadesService,
    private categoriaService: CategoriaService,
    private productoService: ProductoService,
    private mesaService: MesaService
  ) {
  }

  goToHome(): void {
    this.router.navigate(['home']);
  }

  agregarAOrden(theProducto: ProductoModel): void {

    const dialogRef = this.dialog
      .open<AgregarProductoOrdenComponent, GenericDialogData, number>(AgregarProductoOrdenComponent, {
        width: '300px',
        data: {
          payload: {
            producto: theProducto
          }
        }
      });

    dialogRef.afterClosed().subscribe(cantidadIngresada => {
      if (cantidadIngresada) {
        const item = {
          id: 0,
          productoId: theProducto.id,
          productoNombre: theProducto.productoNombre,
          productoPrecio: theProducto.precio,
          cantidad: cantidadIngresada,
          total: theProducto.precio * cantidadIngresada
        };

        this.detalleOrden.push(item);

        this.calcularTotales();
      }
    });

  }

  private calcularTotales(): void {
    let total = 0;
    for (const item of this.detalleOrden) {
      total += item.total;
    }

    this.totalOrden = total;
  }

  regresarACategorias(): void {
    this.mostrarCategorias = true;
  }

  buscarProductos(categoria: CategoriaModel): void {
    this.categoriaActual = categoria;
    this.mostrarBarraProgreso = true;
    this.productoService
      .obtenerProductosPorCategoria(categoria.id)
      .then(productos => {
        this.productos = productos;
        this.mostrarCategorias = false;
      })
      .catch(err => {
        console.error(err);
        this.utilidadesService.showErrorSnackBar('Error al obtener los productos');
      })
      .finally(() => {
        this.mostrarBarraProgreso = false;
      });
  }

  pagar(): void {
    if (!this.clienteNit || !this.clienteNombre || !this.clienteDireccion) {
      this.utilidadesService.showErrorSnackBar('Ingrese los datos del cliente');
      return;
    }

    const dialogRef = this.dialog
      .open<PagarOrdenComponent, GenericDialogData, SeleccionarTipoPagoRespuesta>(PagarOrdenComponent, {
        width: '400px',
        data: {
          payload: {
            montoCompra: this.totalOrden
          }
        }
      });

    dialogRef.afterClosed().subscribe(pago => {
      if(pago) {

      }
    });

  }

  finalizarOrden(): void {
    if (!this.mesaSeleccionada) {
      this.utilidadesService.showErrorSnackBar('Seleccione una mesa');
      return;
    }

  }

  private obtenerCategorias(): void {
    this.mostrarBarraProgreso = true;

    this.categoriaService
      .obtenerCategorias()
      .then(categorias => {
        this.categorias = categorias;
      })
      .catch(err => {
        console.error(err);
        this.utilidadesService.showErrorSnackBar('Error al cargar las categorias');
      })
      .finally(() => {
        this.mostrarBarraProgreso = false;
      });
  }

  private obtenerMesas(): void {
    this.mesaService
      .obtenerMesas()
      .then(mesas => {
        this.mesas = mesas;
      })
      .catch(err => {
        console.error(err);
      });
  }

  ngOnInit(): void {
    this.obtenerCategorias();
    this.obtenerMesas();
  }

}
