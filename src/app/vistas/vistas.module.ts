import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const lazyRoutes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'control-mesas',
    loadChildren: () => import('./control-mesas/control-mesas.module').then(m => m.ControlMesasModule)
  },
  {
    path: 'menu/categorias',
    loadChildren: () => import('./menu-productos/menu-productos.module').then(m => m.MenuProductosModule)
  },
  {
    path: 'ordenes',
    loadChildren: () => import('./ordenes/ordenes.module').then(m => m.OrdenesModule)
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(lazyRoutes)
  ]
})
export class VistasModule {
}
