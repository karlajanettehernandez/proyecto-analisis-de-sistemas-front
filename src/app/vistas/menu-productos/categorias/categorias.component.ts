import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {CategoriaModel, GenericDialogData} from '../../../models/models';
import {CategoriaService} from '../../../services/categoria.service';
import {UtilidadesService} from '../../../services/utilidades.service';
import {CrearEditarCategoriaComponent} from '../crear-editar-categoria/crear-editar-categoria.component';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {

  mostrarBarraProgreso = false;
  searchInput = '';
  categorias: CategoriaModel[] = [];

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private categoriaService: CategoriaService,
    private utilidadesService: UtilidadesService,
  ) {
  }

  goToHome(): void {
    this.router.navigate(['home']);
  }

  buscar(): void {

  }

  crearCategoria(): void {
    const dialogRef = this.dialog
      .open<CrearEditarCategoriaComponent, GenericDialogData, CategoriaModel>(CrearEditarCategoriaComponent, {
        width: '500px',
        data: {
          operationType: 'ADD'
        }
      });

    dialogRef.afterClosed().subscribe(nuevaCategoria => {
      if (nuevaCategoria) {
        this.mostrarBarraProgreso = true;

        this.categoriaService
          .crearCategoria(nuevaCategoria)
          .then(id => {
            if (id > 0) {
              nuevaCategoria.id = id;
              this.utilidadesService.showSuccessSnackBar('Categoria agregada exitosamente!');
              this.categorias.push(nuevaCategoria);
            }
          })
          .catch(err => {
            console.error(err);
            this.utilidadesService.showErrorSnackBar('Error al crear nueva mesa');
          })
          .finally(() => {
            this.mostrarBarraProgreso = false;
          });
      }
    });
  }

  goToProductos(categoria: CategoriaModel) {
    this.router.navigate([`/menu/categorias/${categoria.id}`]);
  }

  ngOnInit(): void {
    this.mostrarBarraProgreso = true;

    this.categoriaService
      .obtenerCategorias()
      .then(categorias => {
        this.categorias = categorias;
      })
      .catch(err => {
        console.error(err);
        this.utilidadesService.showErrorSnackBar('Error al cargar las categorias');
      })
      .finally(() => {
        this.mostrarBarraProgreso = false;
      });
  }

}
