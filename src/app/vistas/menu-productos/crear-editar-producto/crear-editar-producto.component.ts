import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilidadesService} from '../../../services/utilidades.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {GenericDialogData, ProductoModel, ProductoTipoModel} from '../../../models/models';
import {ProductoTipoService} from '../../../services/producto-tipo.service';

@Component({
  selector: 'app-crear-editar-producto',
  templateUrl: './crear-editar-producto.component.html',
  styleUrls: ['./crear-editar-producto.component.scss']
})
export class CrearEditarProductoComponent implements OnInit {

  formulario: FormGroup;

  tipos: ProductoTipoModel[] = [];

  constructor(
    private utilidadesService: UtilidadesService,
    private productoTipoService: ProductoTipoService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GenericDialogData, ProductoModel>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
  ) {

    if (this.data.operationType === 'ADD') {
      this.formulario = this.formBuilder.group({
        productoNombre: ['', Validators.required],
        productoDescripcion: ['', Validators.required],
        precio: [0, Validators.required],
        productoTipoId: [0, Validators.required],
        activo: [true]
      });
    } else {
      const theProducto = this.data.payload.producto as ProductoModel;

      this.formulario = this.formBuilder.group({
        productoNombre: [theProducto.productoNombre, Validators.required],
        productoDescripcion: [theProducto.productoDescripcion, Validators.required],
        precio: [theProducto.precio, Validators.required],
        productoTipoId: [theProducto.productoTipoId, Validators.required],
        activo: [true]
      });
    }

  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar(): void {
    const theProducto: ProductoModel = {
      ...this.formulario.value,
      categoriaId: this.data.payload.categoriaId
    };
    this.dialogRef.close(theProducto);
  }

  ngOnInit(): void {
    this.productoTipoService
      .obtenerProductoTipos()
      .then(tipos => {
        this.tipos = tipos;
      })
      .catch(err => {
        console.error(err);
      });
  }

}
