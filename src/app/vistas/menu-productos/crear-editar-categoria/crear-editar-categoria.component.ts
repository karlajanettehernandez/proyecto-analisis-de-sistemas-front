import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilidadesService} from '../../../services/utilidades.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CategoriaModel, GenericDialogData} from '../../../models/models';

@Component({
  selector: 'app-crear-actualizar-categoria',
  templateUrl: './crear-editar-categoria.component.html',
  styleUrls: ['./crear-editar-categoria.component.scss']
})
export class CrearEditarCategoriaComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private utilidadesService: UtilidadesService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GenericDialogData, CategoriaModel>,
    @Inject(MAT_DIALOG_DATA) public data: GenericDialogData,
  ) {

    if (this.data.operationType === 'ADD') {
      this.formulario = this.formBuilder.group({
        categoriaNombre: ['', Validators.required],
        activo: [true]
      });
    } else {
      this.formulario = this.formBuilder.group({
        categoriaNombre: ['', Validators.required],
        activo: [true]
      });
    }

  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar(): void {
    const nuevaCategoria: CategoriaModel = {...this.formulario.value};
    this.dialogRef.close(nuevaCategoria);
  }

  ngOnInit(): void {
  }

}
