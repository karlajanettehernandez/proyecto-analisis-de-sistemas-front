import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  private unsubscribeAll = new Subject<boolean>();

  userName = '';
  userEmail = '';
  rol = '';

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
  }

  logout(): void {
    // 1. Borramos el token localmente
    this.authenticationService.logout();

    // 2. Redireccionamos al login
    this.router.navigate(['/login']);
  }

  pantallaControlDeMesas(): void {
    this.router.navigate(['/control-mesas']);
  }

  pantallaMenuProductos(): void {
    this.router.navigate(['/menu/categorias']);
  }

  tomarOrden(): void {
    this.router.navigate(['/ordenes/tomar-orden']);
  }

  ngOnInit(): void {
    this.authenticationService
      .userInfo
      .pipe(
        takeUntil(this.unsubscribeAll)
      )
      .subscribe(info => {
        this.userName = info.nombre;
        this.userEmail = info.email;
        this.rol = info.rol;
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

}
