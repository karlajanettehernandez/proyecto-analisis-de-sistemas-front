import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthContentGuard} from './auth-content.guard';
import {PrivateContentGuard} from './private-content.guard';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [AuthContentGuard, PrivateContentGuard]
})
export class GuardsModule { }
