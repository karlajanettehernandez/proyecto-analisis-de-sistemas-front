export interface LoginResultModel {
  loginExitoso: boolean;
  error: string;
  token: string;
}

export interface UserLoginDataModel {
  email: string;
  password: string;
}

export interface UserInfoModel {
  usuarioId: number;
  email: string;
  nombre: string;
  rol: string;
}

export interface GenericDialogData {
  id?: number;
  operationType?: string;
  payload?: any;
}

export interface MesaModel {
  id: number;
  numeroMesa: number;
  cantidadMaximaPersonas: number;
  mesaEstadoId: number;
  mesaEstadoNombre: string;
  fechaCreacion: Date;
  creadoPor: string;
  activo: boolean;
}

export interface CategoriaModel {
  id: number;
  categoriaNombre: string;
}


export interface CategoriaModel {
  id: number;
  activo: boolean;
  creadoPor: string;
  fechaCreacion: Date;
  categoriaNombre: string;
}

export interface ProductoModel {
  id: number;
  activo: boolean;
  creadoPor: string;
  fechaCreacion: Date;
  precio: number;
  productoDescripcion: string;
  productoNombre: string;
  productoTipoId: string;
  categoriaId: number;
}

export interface ProductoTipoModel {
  id: number;
  activo: boolean;
  creadoPor: string;
  fechaCreacion: Date;
  productoTipoNombre: string;
}

export interface OrdenModel {
  id?: number;
  tipoOrden: string;
  mesaId?: number;
  clienteNit?: string;
  clienteNombre?: string;
  clienteDireccion?: string;
  total: number;
  tipoPago?: number;
  facturaId?: number;
  detalles?: OrdenDetalleModel[]
}

export interface OrdenDetalleModel {
  id?: number;
  productoId: number;
  productoNombre: string;
  productoPrecio: number;
  cantidad: number;
  total: number;
}
