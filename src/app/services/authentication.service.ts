import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {LoginResultModel, UserInfoModel, UserLoginDataModel} from '../models/models';

@Injectable()
export class AuthenticationService {

  private isLoggedSubject = new BehaviorSubject<boolean>(false);
  private userInfoSubject = new BehaviorSubject<UserInfoModel>(null);

  isLogged = this.isLoggedSubject.asObservable();
  userInfo = this.userInfoSubject.asObservable();

  constructor(private http: HttpClient) {
  }

  async login(userData: UserLoginDataModel): Promise<string> {
    try {
      const url = `${environment.api}usuario/login`;
      const respuesta = await this.http
        .post<LoginResultModel>(url, userData, {})
        .toPromise();

      if (!respuesta.loginExitoso) {
        return 'Usuario o password incorrectos';
      }

      localStorage.setItem('token', respuesta.token);

      this.verificarLogin();

      return 'OK';
    } catch (e) {
      const errorResponse = e as HttpErrorResponse;

      if (errorResponse.error.error) {
        return errorResponse.error.error;
      }

      console.error(e);
      return '';
    }
  }

  logout(): void {
    localStorage.removeItem('token');
    this.verificarLogin();
  }

  verificarLogin(): void {
    const token = localStorage.getItem('token');
    if (!token) {
      this.isLoggedSubject.next(false);
    } else {
      this.isLoggedSubject.next(true);
      // Decodificamos el token para saber quien esta logueado
      const userData = window.atob(token);

      this.userInfoSubject.next(JSON.parse(userData) as UserInfoModel);
    }
  }
}
