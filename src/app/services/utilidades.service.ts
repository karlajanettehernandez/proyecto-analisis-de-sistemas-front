import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class UtilidadesService {

  constructor(private snackBar: MatSnackBar) {
  }

  showSuccessSnackBar(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 5000,
      verticalPosition: 'top',
      panelClass: ['snackbar-success']
    });
  }

  showErrorSnackBar(message: string): void {
    this.snackBar.open(message, 'OK', {
      duration: 5000,
      verticalPosition: 'top',
      panelClass: ['snackbar-error']
    });
  }

}
