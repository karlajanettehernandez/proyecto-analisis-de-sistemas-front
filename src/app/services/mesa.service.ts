import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MesaModel} from '../models/models';
import {environment} from '../../environments/environment';

@Injectable()
export class MesaService {

  constructor(
    private readonly http: HttpClient,
  ) {
  }

  obtenerMesas(): Promise<MesaModel[]> {
    const token = localStorage.getItem('token');
    const theHeaders = {authorization: `Basic ${token}`};
    const url = `${environment.api}mesa`;

    return this.http
      .get<MesaModel[]>(url, {headers: theHeaders})
      .toPromise();
  }

  crearMesa(mesa: MesaModel): Promise<number> {
    const token = localStorage.getItem('token');
    const theHeaders = {authorization: `Basic ${token}`};
    const url = `${environment.api}mesa`;

    return this.http
      .post<number>(url, mesa, {headers: theHeaders})
      .toPromise();
  }

  actualizarMesa(mesa: MesaModel): Promise<boolean> {
    const token = localStorage.getItem('token');
    const theHeaders = {authorization: `Basic ${token}`};
    const url = `${environment.api}mesa`;

    return this.http
      .put<boolean>(url, mesa, {headers: theHeaders})
      .toPromise();
  }

}
