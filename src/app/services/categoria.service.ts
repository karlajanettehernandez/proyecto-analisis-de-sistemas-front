import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CategoriaModel} from '../models/models';
import {environment} from '../../environments/environment';

@Injectable()
export class CategoriaService {

  constructor(private readonly http: HttpClient) {
  }

  obtenerCategorias(): Promise<CategoriaModel[]> {
    const url = `${environment.api}categoria`;
    return this.http.get<CategoriaModel[]>(url).toPromise();
  }

  crearCategoria(categoria: CategoriaModel): Promise<number> {
    const token = localStorage.getItem('token');
    const theHeaders = {authorization: `Basic ${token}`};

    const url = `${environment.api}categoria`;

    return this.http.post<number>(url, categoria, {headers: theHeaders}).toPromise();
  }

  actualizarCategoria(categoria: CategoriaModel): Promise<boolean> {
    const token = localStorage.getItem('token');
    const theHeaders = {authorization: `Basic ${token}`};

    const url = `${environment.api}categoria`;

    return this.http.put<boolean>(url, categoria, {headers: theHeaders}).toPromise();
  }

}
