import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductoModel} from '../models/models';
import {environment} from '../../environments/environment';

@Injectable()
export class ProductoService {

  constructor(private readonly http: HttpClient) {
  }

  obtenerProductosPorCategoria(theCategoriaId: number): Promise<ProductoModel[]> {
    const url = `${environment.api}producto`;
    const parametros = {
      categoriaId: theCategoriaId.toString()
    };

    return this.http.get<ProductoModel[]>(url, {params: parametros}).toPromise();
  }

  crearProducto(producto: ProductoModel): Promise<number> {
    const token = localStorage.getItem('token');
    const theHeaders = {authorization: `Basic ${token}`};

    const url = `${environment.api}producto`;

    return this.http.post<number>(url, producto, {headers: theHeaders}).toPromise();
  }

  actualizarProducto(producto: ProductoModel): Promise<boolean> {
    const token = localStorage.getItem('token');
    const theHeaders = {authorization: `Basic ${token}`};

    const url = `${environment.api}producto`;

    return this.http.put<boolean>(url, producto, {headers: theHeaders}).toPromise();
  }

}
